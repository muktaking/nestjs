import {
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
  Body,
  Get,
  Param
} from "@nestjs/common";
import { ExamsService } from "./exams.service";
import { CreateExamDto } from "./dto/exam.dto";

@Controller("exams")
export class ExamsController {
  constructor(private readonly examService: ExamsService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async createExam(@Body() createExamDto: CreateExamDto, creator: string) {
    return await this.examService.createExam(createExamDto, creator);
  }

  @Get(":id")
  async findExamById(@Param("id") id) {
    return await this.examService.findExamById(id);
  }
  @Get("questions/:id")
  async findQuestionsByExamId(@Param("id") id) {
    return await this.examService.findQuestionsByExamId(id);
  }
}
